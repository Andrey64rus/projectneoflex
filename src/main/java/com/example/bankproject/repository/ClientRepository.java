package com.example.bankproject.repository;
import com.example.bankproject.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<Client, Integer> {
    Boolean existsByUsername(String username);
}
