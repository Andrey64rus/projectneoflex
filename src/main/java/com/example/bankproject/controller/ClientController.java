package com.example.bankproject.controller;

import com.example.bankproject.exception.SaveClientException;
import com.example.bankproject.util.LinksController;
import com.example.bankproject.model.Client;
import com.example.bankproject.service.impl.ClientServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;


@Controller
public class ClientController {
    private final ClientServiceImpl clientService;

    @Autowired
    public ClientController(ClientServiceImpl clientService) {
        this.clientService = clientService;
    }

    private static final String CLIENT = "client";
    private  static final String ERROR = "Error";
    private static final String ERROR_MESSAGE = "errorMessage";


    @GetMapping(LinksController.LINK_CLIENTS_LIST)
    public String findAllClient(Model model){
        List<Client> client = clientService.findAllClient();
        model.addAttribute(CLIENT, client);
        return LinksController.HTML_CLIENTS_LIST;
    }

    @GetMapping(LinksController.LINK_CLIENT_CREATE)
    public String createClientForm(Client client){
        return LinksController.HTML_CLIENT_CREATE;
    }

    @PostMapping(LinksController.LINK_CLIENT_CREATE)
    public String createClient(@ModelAttribute(CLIENT) @Valid Client client,
                               BindingResult bindingResult,
                               Model model) {
        if (bindingResult.hasErrors()) {
            return createClientForm(client);
        }
        try {
            clientService.saveClient(client);
        } catch (SaveClientException e) {
            model.addAttribute(ERROR_MESSAGE, ERROR + e.getMessage());
            return LinksController.HTML_CLIENT_CREATE;
        }

        return LinksController.RETURN_CLIENTS_LIST;
    }

    @GetMapping(LinksController.LINK_CLIENT_UPDATE)
    public String updateClientForm(@PathVariable("id") Integer id, Model model){
        var client = clientService.findByIdClient(id);
        model.addAttribute(CLIENT, client);
        return LinksController.HTML_CLIENT_UPDATE;
    }

    @PostMapping(LinksController.LINK_CLIENT_UPDATE)
    public String updateClient(@ModelAttribute(CLIENT) @Valid Client client,
                               BindingResult bindingResult,
                               Model model) {
        if (bindingResult.hasErrors()) {
            return LinksController.HTML_CLIENT_UPDATE;
        }
        try{
            clientService.saveClient(client);
        } catch (SaveClientException e) {
            model.addAttribute(ERROR_MESSAGE, ERROR + e.getMessage());
            return LinksController.HTML_ACCOUNT_UPDATE;
        }

        return LinksController.RETURN_CLIENTS_LIST;
    }

    @GetMapping(LinksController.LINK_CLIENT_DELETE)
    public String deleteClient(@PathVariable("id") Integer id)  {
            clientService.deleteByIdClient(id);

        return LinksController.RETURN_CLIENTS_LIST;
    }
}
