package com.example.bankproject.model;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "client")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "username")
    @NotNull(message = "Name is mandatory")
    @Size(min=4, max = 30, message = "Username should be between 4 and 30 characters")
    private String username;

    @Column(name="password")
    @NotNull(message = "Password is mandatory")
    @Size(min = 4, max = 30, message = "Password should be between 2 and 30 characters")
    private String password;

    @Column(name = "birth_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    @NotNull(message = "Birth Date is mandatory")
    private Date birthDate;

    @Column(name="name")
    @NotBlank(message = "Name is mandatory")
    @Size(min = 2, max = 30, message = "Name should be between 2 and 30 characters")
    private String name;

    @Column(name = "surname")
    @NotBlank(message = "Surname is mandatory")
    @Size(min = 2, max = 30, message = "Username should be between 2 and 30 characters")
    private String surname;

    @OneToMany(mappedBy="client", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Account> accounts;


}
