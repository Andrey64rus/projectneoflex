package com.example.bankproject.controller;

import com.example.bankproject.model.AuditHistory;
import com.example.bankproject.service.AuditHistoryService;
import com.example.bankproject.util.LinksController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class AuditHistoryController {

    private AuditHistoryService auditHistoryService;

    @Autowired
    public AuditHistoryController(AuditHistoryService auditHistoryService) {
        this.auditHistoryService = auditHistoryService;
    }

    @GetMapping(LinksController.LINK_AUDITS_HISTORY_LIST)
    public String findAllAuditHistory (Model model) {
        List<AuditHistory> auditHistories = auditHistoryService.findAll();
        model.addAttribute("audits", auditHistories);
        return LinksController.HTML_AUDITS_LIST;
    }
}
