package com.example.bankproject.service;

import com.example.bankproject.exception.BankTransactionException;
import com.example.bankproject.exception.DateAccountException;
import com.example.bankproject.model.Account;

import java.util.List;

public interface AccountService {
    Account findByIdAccount(Integer id);
    Account saveAccount(Account account) throws DateAccountException;
    void deleteByIdAccount(Integer id);
    List<Account> findAccountsByClientId(Integer id);
    void addAmount(Integer id, double amount) throws BankTransactionException;
    void sendMoney(Integer id, Integer toAccountNumber, Double amount) throws BankTransactionException;
}
