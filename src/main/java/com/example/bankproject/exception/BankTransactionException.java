package com.example.bankproject.exception;

public class BankTransactionException extends Exception {

    public BankTransactionException(String message) {
        super(message);
    }
}
