package com.example.bankproject.service;


import com.example.bankproject.exception.SaveClientException;
import com.example.bankproject.model.Client;

import java.util.List;

public interface ClientService {
    Client findByIdClient(Integer id);
    List<Client> findAllClient();
    Client saveClient(Client client) throws SaveClientException;
    void deleteByIdClient(Integer id);
    Boolean usernameAlreadyExists(String username);
}
