package com.example.bankproject.exception;

public class SaveClientException extends Exception {
    public SaveClientException(String message) {
        super(message);
    }
}
