package com.example.bankproject.model.enums;

public enum Status {
    OPEN,
    CLOSED,
    SUSPEND;

    Status() {
    }
}
