package com.example.bankproject.model.enums;

public enum AuditAction {
    CREATE,
    UPDATE,
    DELETE,
    SENDER,
    RECIPIENT;

    AuditAction() {
    }
}
