package com.example.bankproject.util;

public class LinksController {
    public static final String HTML_CLIENTS_LIST = "client-list";
    public static final String HTML_CLIENT_CREATE = "client-create";
    public static final String HTML_CLIENT_UPDATE  = "client-update";

    public static final String RETURN_CLIENTS_LIST = "redirect:/clients";

    public static final String LINK_CLIENTS_LIST = "/clients";
    public static final String LINK_CLIENT_CREATE = "/client-create";
    public static final String LINK_CLIENT_DELETE = "/client-delete/{id}";
    public static final String LINK_CLIENT_UPDATE = "/client-update/{id}";

    public static final String HTML_ACCOUNTS_LIST = "account-list";
    public static final String HTML_ACCOUNT_CREATE = "account-create";
    public static final String HTML_ACCOUNT_UPDATE = "account-update";
    public static final String HTML_SEND_MONEY = "send-money-form";

    public static final String RETURN_ACCOUNTS_LIST = "redirect:/account-list/{clientId}";

    public static final String LINK_ACCOUNTS_LIST = "account-list/{clientId}";
    public static final String LINK_ACCOUNTS_CREATE = "/account-create/{clientId}";
    public static final String LINK_ACCOUNT_UPDATE = "/account-update/{clientId}/{accountId}";
    public static final String LINK_ACCOUNT_DELETE = "/account-delete/{clientId}/{id}";
    public static final String LINK_SEND_MONEY = "/send-money/{clientId}/{accountId}";

    public static final String HTML_AUDITS_LIST ="audits-list";

    public static final String LINK_AUDITS_HISTORY_LIST = "/audits";

    private LinksController() {
    }
}
