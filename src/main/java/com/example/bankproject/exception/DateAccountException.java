package com.example.bankproject.exception;

public class DateAccountException extends Exception{
    public  DateAccountException(String message) {
        super(message);
    }
}
