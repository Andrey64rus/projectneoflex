package com.example.bankproject.service;

import com.example.bankproject.model.AuditHistory;
import com.example.bankproject.model.enums.AuditAction;
import com.example.bankproject.model.enums.ObjectType;
import java.util.Date;
import java.util.List;

public interface AuditHistoryService {
    AuditHistory saveAuditHistory(AuditHistory auditHistory);
    List<AuditHistory> findAll();
    AuditHistory addAuditHistory(Integer objectId,
                                 ObjectType objectType,
                                 Date actionDate,
                                 AuditAction auditAction,
                                 Double newValue);
}
