package com.example.bankproject.service.impl;


import com.example.bankproject.exception.BankTransactionException;
import com.example.bankproject.exception.DateAccountException;
import com.example.bankproject.model.Account;
import com.example.bankproject.model.enums.AuditAction;
import com.example.bankproject.model.enums.ObjectType;
import com.example.bankproject.repository.AccountRepository;
import com.example.bankproject.service.AccountService;
import com.example.bankproject.service.AuditHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Date;
import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {

    private AccountRepository accountRepository;

    private AuditHistoryService auditHistoryService;


    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository, AuditHistoryService auditHistoryService) {
        this.accountRepository = accountRepository;
        this.auditHistoryService = auditHistoryService;
    }


    @Override
    public Account findByIdAccount(Integer id) {
        return accountRepository.getOne(id);
    }

    @Override
    @Transactional
    public Account saveAccount(Account account) throws DateAccountException {
        if (account.getOpenDate().compareTo(account.getCloseDate()) > 0) {
            throw new DateAccountException("The end date should be bigger than the start date!");
        }
        return accountRepository.save(account);
    }

    @Override
    public void deleteByIdAccount(Integer id) {
        accountRepository.deleteById(id);
        var auditHistory = auditHistoryService.addAuditHistory(id,ObjectType.ACCOUNT, new Date(),
                AuditAction.DELETE, (double) 0);
        auditHistoryService.saveAuditHistory(auditHistory);
    }

    @Override
    public List<Account> findAccountsByClientId(Integer id) {
        return accountRepository.findAccountsByClient_Id(id);
    }

    @Override
    @Transactional
    public void addAmount(Integer id, double amount)
            throws BankTransactionException {
        var account = accountRepository.getOne(id);


        if (account == null) {
            throw new BankTransactionException("Account not found " + id);
        }
        double newBalance = account.getBalance() + amount;
        if (account.getBalance() + amount < 0) {
            throw new BankTransactionException("It is not possible to transfer money to the account number '"
                    + id + " 'this is not enough, everything is on the account ("
                    + account.getBalance() + ")");
        }


        account.setBalance(newBalance);
        accountRepository.save(account);

    }

    @Override
    @Transactional
    public void sendMoney(Integer fromAccountId, Integer toAccountId, Double amount)
            throws BankTransactionException {
        var sender = accountRepository.getOne(fromAccountId);
        var recipient = accountRepository.getOne(toAccountId);

        addAmount(fromAccountId, -amount);
        addAmount(toAccountId, amount);

        var auditSender = auditHistoryService.addAuditHistory(fromAccountId,
                ObjectType.ACCOUNT,
                new Date(),
                AuditAction.SENDER,
                sender.getBalance());

        var auditRecipient = auditHistoryService.addAuditHistory(toAccountId,
                ObjectType.ACCOUNT,
                new Date(),
                AuditAction.RECIPIENT,
                recipient.getBalance());

        auditHistoryService.saveAuditHistory(auditSender);
        auditHistoryService.saveAuditHistory(auditRecipient);
    }
}
