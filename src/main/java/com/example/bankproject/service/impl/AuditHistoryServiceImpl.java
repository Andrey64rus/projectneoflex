package com.example.bankproject.service.impl;

import com.example.bankproject.model.AuditHistory;
import com.example.bankproject.model.enums.AuditAction;
import com.example.bankproject.model.enums.ObjectType;
import com.example.bankproject.repository.AuditHistoryRepository;
import com.example.bankproject.service.AuditHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Date;
import java.util.List;

@Service
public class AuditHistoryServiceImpl implements AuditHistoryService {

    private AuditHistoryRepository auditHistoryRepository;

    @Autowired
    public AuditHistoryServiceImpl(AuditHistoryRepository auditHistoryRepository) {
        this.auditHistoryRepository = auditHistoryRepository;
    }

    @Override
    public AuditHistory saveAuditHistory(AuditHistory auditHistory) {
        return auditHistoryRepository.save(auditHistory);
    }

    @Override
    public List<AuditHistory> findAll() {
        return auditHistoryRepository.findAll();
    }

    @Override
    public AuditHistory addAuditHistory(Integer objectId,
                                        ObjectType objectType,
                                        Date actionDate,
                                        AuditAction auditAction,
                                        Double newValue) {
        return AuditHistory.builder().
                objectId(objectId)
                .objectType(objectType)
                .actionDate(actionDate)
                .auditAction(auditAction)
                .newValue(newValue)
                .build();
    }
}
