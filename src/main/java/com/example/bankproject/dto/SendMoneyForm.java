package com.example.bankproject.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SendMoneyForm {

    private Integer toAccountId;

    private Double amount;
}
