package com.example.bankproject.model;

import com.example.bankproject.model.enums.AuditAction;
import com.example.bankproject.model.enums.ObjectType;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "audit")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AuditHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "object_id")
    private Integer objectId;

    @Column(name = "object_type")
    private ObjectType objectType;

    @Column(name = "action_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss", iso = DateTimeFormat.ISO.DATE_TIME)
    private Date actionDate;

    @Column(name = "audit_action")
    private AuditAction auditAction;

    @Column(name = "new_value")
    private Double newValue;
}
