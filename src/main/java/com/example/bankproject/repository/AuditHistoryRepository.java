package com.example.bankproject.repository;

import com.example.bankproject.model.AuditHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuditHistoryRepository extends JpaRepository<AuditHistory, Integer> {
}
