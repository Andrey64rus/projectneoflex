package com.example.bankproject.controller;

import com.example.bankproject.dto.SendMoneyForm;
import com.example.bankproject.exception.BankTransactionException;
import com.example.bankproject.exception.DateAccountException;
import com.example.bankproject.model.Account;
import com.example.bankproject.model.enums.AuditAction;
import com.example.bankproject.model.enums.ObjectType;
import com.example.bankproject.service.AccountService;
import com.example.bankproject.service.AuditHistoryService;
import com.example.bankproject.service.ClientService;
import com.example.bankproject.util.LinksController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@Controller
public class AccountController {

    private AccountService accountService;
    private AuditHistoryService auditHistoryService;
    private ClientService clientService;

    private static final String CLIENT_ID = "clientId";
    private static final String ACCOUNT_ID = "accountId";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String ERROR = "Error";
    private static final String CLIENT = "client";
    private static final String ACCOUNT = "account";

    @Autowired
    public AccountController(AccountService accountService, ClientService clientService, AuditHistoryService auditHistoryService) {
        this.accountService = accountService;
        this.clientService = clientService;
        this.auditHistoryService = auditHistoryService;
    }

    @GetMapping(LinksController.LINK_ACCOUNTS_LIST)
    public String findAllAccountByClientId(@PathVariable(CLIENT_ID) Integer clientId, Model model) {
        List<Account> accountList = accountService.findAccountsByClientId(clientId);
        model.addAttribute("accounts", accountList);
        model.addAttribute(CLIENT_ID, clientId);
        model.addAttribute(CLIENT, clientService.findByIdClient(clientId));
        return LinksController.HTML_ACCOUNTS_LIST;
    }

    @GetMapping(LinksController.LINK_ACCOUNTS_CREATE)
    public String createAccountForm(@PathVariable(CLIENT_ID) Integer clientId, Account account, Model model) {
        model.addAttribute(CLIENT_ID, clientId);
        return LinksController.HTML_ACCOUNT_CREATE;
    }

    @PostMapping(LinksController.LINK_ACCOUNTS_CREATE)
    public String createAccount(@PathVariable(CLIENT_ID) Integer clientId, @Valid Account account,
                                BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return createAccountForm(clientId, account, model);
        }
        try {
            var client = clientService.findByIdClient(clientId);
            account.setClient(client);
            accountService.saveAccount(account);
        } catch (DateAccountException e) {
            model.addAttribute(ERROR_MESSAGE, ERROR + e.getMessage());
            return LinksController.HTML_ACCOUNT_CREATE;
        }
        var auditHistory = auditHistoryService.addAuditHistory(account.getId(),
                ObjectType.ACCOUNT, new Date(),
                AuditAction.CREATE, account.getBalance());
        auditHistoryService.saveAuditHistory(auditHistory);
        return LinksController.RETURN_ACCOUNTS_LIST;
    }

    @GetMapping(LinksController.LINK_ACCOUNT_UPDATE)
    public String updateAccountForm(@PathVariable(ACCOUNT_ID) Integer accountId,
                                    @PathVariable(CLIENT_ID) Integer clientId,
                                    Account accounts, Model model) {
        var account = accountService.findByIdAccount(accountId);
        model.addAttribute(ACCOUNT, account);
        model.addAttribute(CLIENT_ID, clientId);
        model.addAttribute(ACCOUNT_ID, accountId);
        return LinksController.HTML_ACCOUNT_UPDATE;
    }

    @PostMapping(LinksController.LINK_ACCOUNT_UPDATE)
    public String updateAccount(@PathVariable(ACCOUNT_ID) Integer accountId,
                                @PathVariable(CLIENT_ID) Integer clientId,
                                @Valid Account account, BindingResult bindingResult, Model model) {
        account.setClient(clientService.findByIdClient(clientId));
        if (bindingResult.hasErrors()) {
            return LinksController.HTML_ACCOUNT_UPDATE;
        }
        try {
            accountService.saveAccount(account);
        } catch (DateAccountException e) {
            model.addAttribute(ERROR_MESSAGE, ERROR + e.getMessage());
            return LinksController.HTML_ACCOUNT_UPDATE;
        }

        var auditHistory = auditHistoryService.addAuditHistory(account.getId(),
                ObjectType.ACCOUNT, new Date(),
                AuditAction.UPDATE, account.getBalance());
        auditHistoryService.saveAuditHistory(auditHistory);
        return LinksController.RETURN_ACCOUNTS_LIST;
    }

    @GetMapping(LinksController.LINK_ACCOUNT_DELETE)
    public String deleteAccount(@PathVariable(CLIENT_ID) Integer clientId,
                                @PathVariable("id") Integer accountId) {
        accountService.deleteByIdAccount(accountId);
        return LinksController.RETURN_ACCOUNTS_LIST;
    }

    @GetMapping(LinksController.LINK_SEND_MONEY)
    public String viewSendMoneyForm(@PathVariable(CLIENT_ID) Integer clientId,
                                    @PathVariable(ACCOUNT_ID) Integer accountId, Model model,
                                    SendMoneyForm sendMoneyForm) {
        model.addAttribute(CLIENT_ID, clientId);
        model.addAttribute(ACCOUNT_ID, accountId);
        model.addAttribute(CLIENT, clientService.findByIdClient(clientId));
        model.addAttribute(ACCOUNT, accountService.findByIdAccount(accountId));
        return LinksController.HTML_SEND_MONEY;
    }

    @PostMapping(LinksController.LINK_SEND_MONEY)
    public String processSendMoney(@PathVariable(CLIENT_ID) Integer clientId,
                                   @PathVariable(ACCOUNT_ID) Integer accountId,
                                   SendMoneyForm sendMoneyForm, Model model) {
        try {
            var client = clientService.findByIdClient(clientId);

            if (!(client.getAccounts().stream().anyMatch(a -> a.getId().equals(accountId))))
                throw new BankTransactionException("The account does not match this client");

            if (sendMoneyForm.getToAccountId() == null) {
                throw new BankTransactionException("Enter the Account Number!");
            }
            if (sendMoneyForm.getAmount() == null) {
                throw new BankTransactionException("Enter the Amount!");
            }

            accountService.sendMoney(accountId,
                    sendMoneyForm.getToAccountId(),
                    sendMoneyForm.getAmount());
        } catch (BankTransactionException e) {
            model.addAttribute(ERROR_MESSAGE, ERROR + e.getMessage());
            model.addAttribute(CLIENT, clientService.findByIdClient(clientId));
            model.addAttribute(ACCOUNT, accountService.findByIdAccount(accountId));
            return LinksController.HTML_SEND_MONEY;
        }
        return LinksController.RETURN_ACCOUNTS_LIST;
    }
}
