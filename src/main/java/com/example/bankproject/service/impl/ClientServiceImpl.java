package com.example.bankproject.service.impl;

import com.example.bankproject.exception.SaveClientException;
import com.example.bankproject.model.Client;
import com.example.bankproject.repository.ClientRepository;
import com.example.bankproject.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;

    @Autowired
    public ClientServiceImpl(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Override
    public Client findByIdClient(Integer id){
        return clientRepository.getOne(id);
    }

    @Override
    public List<Client> findAllClient(){
        return clientRepository.findAll();
    }

    @Override
    public Client saveClient(Client client) throws SaveClientException {
        if (client.getName()!=null && client.getSurname()!= null) {

            String name = client.getName();
            client.setName(name.substring(0,1).toUpperCase() + name.substring(1));

            String surname = client.getSurname();
            client.setSurname(surname.substring(0,1).toUpperCase() + surname.substring(1));
        }

        String newUsername = client.getUsername();
        if (usernameAlreadyExists(newUsername)) {
            throw new SaveClientException(" This username already exists: " + newUsername);
        }
        return clientRepository.save(client);
    }

    @Override
    public Boolean usernameAlreadyExists(String username) {
        return clientRepository.existsByUsername(username);
    }

    @Override
    public void deleteByIdClient(Integer id) {
            clientRepository.deleteById(id);
    }
}
